# HomeTown-iOS
The HomeTown iOS application (built for BoilerMake 2015).

HomeTown is an essentially a contacts app that allows you to identify where people are from and then view their
hometowns altogether on a map.

Co-created with [Sam Albert](https://github.com/samwalbert) and [Noah Rinehart](https://github.com/noahrinehart) for
BoilerMake 2015.

- Awarded the Trunk Club prize : Best app that connects people.
