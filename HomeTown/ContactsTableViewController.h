//
//  ContactsTableViewController.h
//  HomeTown
//
//  Created by Sanjeet Suhag on 10/17/15.
//  Copyright © 2015 Hometown. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ViewContactTableViewController.h"

@interface ContactsTableViewController : UITableViewController

@end
