//
//  AddContactTableViewController.h
//  HomeTown
//
//  Created by Sanjeet Suhag on 10/17/15.
//  Copyright © 2015 Hometown. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddContactTableViewController : UITableViewController <UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    UIImagePickerController *imagePickerController;
}

@property (weak, nonatomic) IBOutlet UIButton *addPhotoButton;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *hometownTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *noteTextField;

- (IBAction)addPhoto:(id)sender;

@end
