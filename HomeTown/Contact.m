//
//  Contact.m
//  HomeTown
//
//  Created by Sanjeet Suhag on 10/17/15.
//  Copyright © 2015 Hometown. All rights reserved.
//

#import "Contact.h"

@implementation Contact

- (id)initWithName:(NSString *)name hometown:(NSString *)hometown phoneNumber:(NSString *)phoneNo lat:(double)latitude lon:(double)longitude {
   
    self = [super init];
    if ( !self ) {
        return nil;
    }
    
    self.name = name;
    self.hometown = hometown;
    self.phoneNumber = phoneNo;
    self.latitude = latitude;
    self.longitude = longitude;
    
    return self;
}

@end
