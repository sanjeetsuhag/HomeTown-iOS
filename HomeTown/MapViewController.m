//
//  MapViewController.m
//  HomeTown
//
//  Created by Sanjeet Suhag on 10/17/15.
//  Copyright © 2015 Hometown. All rights reserved.
//

#import "MapViewController.h"

@interface MapViewController () <CLLocationManagerDelegate>

@property (strong, nonatomic) NSMutableArray *contacts;
@property (strong, nonatomic) NSMutableArray *hometownContacts;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *infoButton;
@property (strong, nonatomic) NSMutableArray *locations;

@property int infoCount;

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _infoCount = 0;
    
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Contact"];
    self.contacts = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    _hometownContacts = [[NSMutableArray alloc] init];
    _locations = [[NSMutableArray alloc] init];
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    [_locationManager requestWhenInUseAuthorization];
    [_locationManager startUpdatingLocation];
    
    _mapView.showsUserLocation = YES;
    
    for (NSManagedObject *contact in _contacts) {
        if ([contact valueForKey:@"hometown"]) {
            [_hometownContacts addObject:contact];
        }
    }
    
    for (NSManagedObject *contact in _hometownContacts) {
        CLLocationCoordinate2D coordinate;
        coordinate.latitude = [[contact valueForKey:@"latitude"] doubleValue];
        coordinate.longitude = [[contact valueForKey:@"longitude"] doubleValue];
        [_locations addObject:[[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude]];
        
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        annotation.coordinate = coordinate;
        annotation.title = [contact valueForKey:@"name"];
        annotation.subtitle = [contact valueForKey:@"hometown"];
        [_mapView addAnnotation:annotation];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    if (_hometownContacts.count < 2) self.navigationItem.rightBarButtonItem.enabled = NO;
    else self.navigationItem.rightBarButtonItem.enabled = YES;
    [super viewDidAppear:animated];
}

#pragma mark - Core Data

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

#pragma mark - MKMapViewDelegate

//- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
//    if ([annotation isKindOfClass:[Contact class]]) {
//        NSString *identifier = @"pin";
//        MKPinAnnotationView *pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
//    }
//}

- (IBAction)showInfo:(id)sender {
    
    NSArray *info = @[[self locationWithMostDistanceBetweenThem], [self locationWithLeastDistanceBetweenThem], [self averageDistance], [self maximumLocals]];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Info"
                                                                             message:info[_infoCount++]
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (NSString *)locationWithMostDistanceBetweenThem {
    double maxDistance = 0;
    NSString *maxDistanceString = @"";
    for (int i = 0; i < _hometownContacts.count; i++) {
        for (int j = 0; j < _hometownContacts.count; j++) {
            CLLocation *location1 = [[CLLocation alloc] initWithLatitude:[[[_hometownContacts objectAtIndex:i] valueForKey:@"latitude"] doubleValue] longitude:[[[_hometownContacts objectAtIndex:i] valueForKey:@"longitude"] doubleValue]];
            CLLocation *location2 = [[CLLocation alloc] initWithLatitude:[[[_hometownContacts objectAtIndex:j] valueForKey:@"latitude"] doubleValue] longitude:[[[_hometownContacts objectAtIndex:j] valueForKey:@"longitude"] doubleValue]];
            
            if ([location1 distanceFromLocation:location2] > maxDistance) {
                maxDistance = [location1 distanceFromLocation:location2];
                maxDistanceString = [NSString stringWithFormat:@"%@ and %@ live the farthest apart with %f miles between their hometowns.", [[_hometownContacts objectAtIndex:i] valueForKey:@"name"], [[_hometownContacts objectAtIndex:j] valueForKey:@"name"], ([location1 distanceFromLocation:location2] / 1000) * 0.62137];
            }
        }
    }
    return maxDistanceString;
}

- (NSString *)locationWithLeastDistanceBetweenThem {
    double leastDistance = INT_MAX;
    NSString *leastDistanceString = @"";
    for (int i = 0; i < _hometownContacts.count; i++) {
        for (int j = 0; j < _hometownContacts.count; j++) {
            if([[[_hometownContacts objectAtIndex:i] valueForKey:@"hometown"] isEqualToString:[[_hometownContacts objectAtIndex:j] valueForKey:@"hometown"]])
                continue;
            
            CLLocation *location1 = [[CLLocation alloc] initWithLatitude:[[[_hometownContacts objectAtIndex:i] valueForKey:@"latitude"] doubleValue] longitude:[[[_hometownContacts objectAtIndex:i] valueForKey:@"longitude"] doubleValue]];
            CLLocation *location2 = [[CLLocation alloc] initWithLatitude:[[[_hometownContacts objectAtIndex:j] valueForKey:@"latitude"] doubleValue] longitude:[[[_hometownContacts objectAtIndex:j] valueForKey:@"longitude"] doubleValue]];
            
            if ([location1 distanceFromLocation:location2] < leastDistance) {
                leastDistance = [location1 distanceFromLocation:location2];
                leastDistanceString = [NSString stringWithFormat:@"%@ and %@ live the closest with %f miles between their hometowns.", [[_hometownContacts objectAtIndex:i] valueForKey:@"name"], [[_hometownContacts objectAtIndex:j] valueForKey:@"name"], ([location1 distanceFromLocation:location2] / 1000) * 0.62137];
            }
        }
    }
    return leastDistanceString;
}

- (NSString *)averageDistance {
    double sum = 0;
    int count = 0;
    for (int i = 0; i < _hometownContacts.count; i++) {
        for (int j = 0; j < _hometownContacts.count; j++) {
            if([[[_hometownContacts objectAtIndex:i] valueForKey:@"hometown"] isEqualToString:[[_hometownContacts objectAtIndex:j] valueForKey:@"hometown"]])
                continue;
            
            CLLocation *location1 = [[CLLocation alloc] initWithLatitude:[[[_hometownContacts objectAtIndex:i] valueForKey:@"latitude"] doubleValue] longitude:[[[_hometownContacts objectAtIndex:i] valueForKey:@"longitude"] doubleValue]];
            CLLocation *location2 = [[CLLocation alloc] initWithLatitude:[[[_hometownContacts objectAtIndex:j] valueForKey:@"latitude"] doubleValue] longitude:[[[_hometownContacts objectAtIndex:j] valueForKey:@"longitude"] doubleValue]];
            
            sum += ([location1 distanceFromLocation:location2] / 1000) * 0.62137;
            count++;
        }
    }
    return [NSString stringWithFormat:@"Average distance between all hometowns is %f", sum / count];
}

- (NSString *)maximumLocals {
    int highestLocalCount = 0;
    NSString *highestLocalHomeTown = @"";
    for (int i = 0; i < _hometownContacts.count; i++) {
        int localCount = 0;
        for (int j = 0; j < _hometownContacts.count; j++) {
            if([[[_hometownContacts objectAtIndex:i] valueForKey:@"hometown"] isEqualToString:[[_hometownContacts objectAtIndex:j] valueForKey:@"hometown"]]) {
                localCount++;
                if (localCount > highestLocalCount) highestLocalCount = localCount;
                highestLocalHomeTown = [[_hometownContacts objectAtIndex:i] valueForKey:@"hometown"];
            }
        }
    }
    return [NSString stringWithFormat:@"%@ is the most common hometown. %d of your contacts come from %@", highestLocalHomeTown, highestLocalCount, highestLocalHomeTown];
}

@end
