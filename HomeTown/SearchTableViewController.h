//
//  SearchTableViewController.h
//  HomeTown
//
//  Created by Sanjeet Suhag on 10/17/15.
//  Copyright © 2015 Hometown. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol hometownProtocol <NSObject>

- (void)getHomeTown:(MKMapItem *)hometown;

@end

@interface SearchTableViewController : UITableViewController

@property (assign, nonatomic) id delegate;

@end
