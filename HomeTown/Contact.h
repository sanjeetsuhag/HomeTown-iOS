//
//  Contact.h
//  HomeTown
//
//  Created by Sanjeet Suhag on 10/17/15.
//  Copyright © 2015 Hometown. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contact : NSObject <MKAnnotation>

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *hometown;
@property (strong, nonatomic) NSString *phoneNumber;
@property double latitude;
@property double longitude;
@property (nonatomic) CLLocationCoordinate2D coordinate;

- (id)initWithName:(NSString *)name hometown:(NSString *)hometown phoneNumber:(NSString *)phoneNo lat:(double)latitude lon:(double)longitude;

@end
