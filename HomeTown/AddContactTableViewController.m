//
//  AddContactTableViewController.m
//  HomeTown
//
//  Created by Sanjeet Suhag on 10/17/15.
//  Copyright © 2015 Hometown. All rights reserved.
//

#import "AddContactTableViewController.h"
#import "SearchTableViewController.h"

@interface AddContactTableViewController () <hometownProtocol>

@property BOOL addedPhoto;
@property (strong, nonatomic) MKMapItem *hometown;

@end

@implementation AddContactTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.nameTextField.delegate = self;
    self.hometownTextField.delegate = self;
    self.phoneNumberTextField.delegate = self;
    
    self.addPhotoButton.clipsToBounds = YES;
    [self.addPhotoButton setImage:[UIImage imageNamed:@"EmptyContact"] forState:UIControlStateNormal];
    self.addPhotoButton.layer.cornerRadius = self.addPhotoButton.frame.size.height / 2;
    self.addPhotoButton.layer.borderWidth = 2;
    self.addPhotoButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.addedPhoto = NO;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneAdding:)];
    self.navigationItem.rightBarButtonItem.enabled = FALSE;
    
    [self.nameTextField addTarget:self action:@selector(textFieldDidEdit:) forControlEvents:UIControlEventEditingChanged];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 2) {
        SearchTableViewController *vc = [[SearchTableViewController alloc] init];
        vc.delegate = self;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == self.nameTextField && ![textField.text isEqualToString:@""]) {
        self.navigationItem.rightBarButtonItem.enabled = TRUE;
    } else if(textField == self.hometownTextField) {
        
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == self.nameTextField && ![textField.text isEqualToString:@""]) {
        self.navigationItem.rightBarButtonItem.enabled = TRUE;
    }
    [self.view endEditing:YES];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *) textField
{
    if (textField != _hometownTextField) return YES;
    SearchTableViewController *vc = [[SearchTableViewController alloc] init];
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
    return NO;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

# pragma mark - Core Data

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

#pragma mark - Selectors

- (void)doneAdding:(id)sender {
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Create a new managed object
    NSManagedObject *newContact = [NSEntityDescription insertNewObjectForEntityForName:@"Contact" inManagedObjectContext:context];
    [newContact setValue:self.nameTextField.text forKey:@"name"];
    [newContact setValue:self.hometownTextField.text forKey:@"hometown"];
    [newContact setValue:self.phoneNumberTextField.text forKey:@"phone"];
    [newContact setValue:self.noteTextField.text forKey:@"note"];
    if (_hometown) {
        [newContact setValue:[NSNumber numberWithDouble:_hometown.placemark.location.coordinate.latitude] forKey:@"latitude"];
        [newContact setValue:[NSNumber numberWithDouble:_hometown.placemark.location.coordinate.longitude] forKey:@"longitude"];
    }
    if (self.addedPhoto) [newContact setValue:UIImagePNGRepresentation(self.addPhotoButton.currentImage) forKey:@"photo"];
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Error saving contact : %@ %@", error, [error localizedDescription]);
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)textFieldDidEdit:(id)sender {
    if (![self.nameTextField.text isEqualToString:@""]) {
        self.navigationItem.rightBarButtonItem.enabled = TRUE;
    }
}

#pragma mark - Selectors

- (IBAction)addPhoto:(id)sender {
    [self.view endEditing:YES];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Add Photo"
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"Camera"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                             imagePickerController = [[UIImagePickerController alloc] init];
                                                             imagePickerController.delegate = self;
                                                             imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                                                             [self presentViewController:imagePickerController animated:YES completion:nil];
                                                         }];
    UIAlertAction *pictureAction = [UIAlertAction actionWithTitle:@"Gallery"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * _Nonnull action) {
                                                              imagePickerController = [[UIImagePickerController alloc] init];
                                                              imagePickerController.delegate = self;
                                                              imagePickerController.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
                                                              [self presentViewController:imagePickerController animated:YES completion:nil];
                                                          }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleDestructive
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                            
                                                         }];
    [alertController addAction:cameraAction];
    [alertController addAction:pictureAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    [self.addPhotoButton setImage:[info objectForKey:UIImagePickerControllerOriginalImage] forState:UIControlStateNormal];
    self.addedPhoto = YES;
}

- (void)getHomeTown:(MKMapItem *)hometown {
    self.hometown = hometown;
    self.hometownTextField.text = hometown.name;
}
                                               
@end
