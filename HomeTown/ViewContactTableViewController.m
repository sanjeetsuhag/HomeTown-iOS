//
//  ViewContactTableViewController.m
//  HomeTown
//
//  Created by Sanjeet Suhag on 10/17/15.
//  Copyright © 2015 Hometown. All rights reserved.
//

#import "ViewContactTableViewController.h"

@interface ViewContactTableViewController () <hometownProtocol>

@property BOOL existsPhoto;
@property (strong, nonatomic) MKMapItem *hometown;

@end

@implementation ViewContactTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.nameTextField.delegate = self;
    self.hometownTextField.delegate = self;
    self.phoneNumberTextField.delegate = self;
    self.noteTextField.delegate = self;
    
    self.photoButton.clipsToBounds = YES;
    self.photoButton.layer.cornerRadius = self.photoButton.frame.size.height / 2;
    self.photoButton.layer.borderWidth = 2;
    self.photoButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    if ([self.contact valueForKey:@"name"]) {
        [self.nameTextField setText:[self.contact valueForKey:@"name"]];
    }
    
    if ([self.contact valueForKey:@"hometown"]) {
        [self.hometownTextField setText:[self.contact valueForKey:@"hometown"]];
    }
    
    if ([self.contact valueForKey:@"phone"]) {
        [self.phoneNumberTextField setText:[self.contact valueForKey:@"phone"]];
    }
    
    if ([self.contact valueForKey:@"note"]) {
        [self.noteTextField setText:[self.contact valueForKey:@"note"]];
    }
    
    if ([self.contact valueForKey:@"photo"]) {
        self.existsPhoto = YES;
        [self.photoButton setImage:[UIImage imageWithData:[self.contact valueForKey:@"photo"]] forState:UIControlStateNormal];
    } else {
        self.existsPhoto = NO;
        [self.photoButton setImage:[UIImage imageNamed:@"EmptyContact"] forState:UIControlStateNormal];
    }
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneEditing:)];
    self.navigationItem.rightBarButtonItem.enabled = FALSE;
    
    [self.nameTextField addTarget:self action:@selector(nameDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.hometownTextField addTarget:self action:@selector(hometownDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.phoneNumberTextField addTarget:self action:@selector(phoneNumberDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.noteTextField addTarget:self action:@selector(noteDidChange:) forControlEvents:UIControlEventEditingChanged];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Selectors

- (void)nameDidChange:(id)sender {
    if ([self.nameTextField.text isEqualToString:@""]) {
        self.navigationItem.rightBarButtonItem.enabled = FALSE;
    } else {
        self.navigationItem.rightBarButtonItem.enabled = TRUE;
    }
}

- (void)hometownDidChange:(id)sender {
    self.navigationItem.rightBarButtonItem.enabled = TRUE;
}

- (void)phoneNumberDidChange:(id)sender {
    self.navigationItem.rightBarButtonItem.enabled = TRUE;
}

- (void)noteDidChange:(id)sender {
    self.navigationItem.rightBarButtonItem.enabled = TRUE;
}

# pragma mark - Core Data

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)doneEditing:(id)sender {
    NSManagedObjectContext *context = [self managedObjectContext];
    
    if (self.contact) {
        [self.contact setValue:self.nameTextField.text forKey:@"name"];
        [self.contact setValue:self.hometownTextField.text forKey:@"hometown"];
        [self.contact setValue:self.phoneNumberTextField.text forKey:@"phone"];
        [self.contact setValue:self.noteTextField.text forKey:@"note"];
        if (self.existsPhoto) [self.contact setValue:UIImagePNGRepresentation(self.photoButton.currentImage) forKey:@"photo"];
        if (_hometown) {
            [self.contact setValue:[NSNumber numberWithInteger:_hometown.placemark.location.coordinate.latitude] forKey:@"latitude"];
            [self.contact setValue:[NSNumber numberWithInteger:_hometown.placemark.location.coordinate.longitude] forKey:@"longitude"];
        }
    }
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Error saving contact : %@ %@", error, [error localizedDescription]);
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *) textField
{
    if (textField != _hometownTextField) return YES;
    SearchTableViewController *vc = [[SearchTableViewController alloc] init];
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
    return NO;
}

- (IBAction)changePhoto:(id)sender {
    [self.view endEditing:YES];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Add Photo"
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"Camera"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                             imagePickerController = [[UIImagePickerController alloc] init];
                                                             imagePickerController.delegate = self;
                                                             imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                                                             [self presentViewController:imagePickerController animated:YES completion:nil];
                                                         }];
    UIAlertAction *pictureAction = [UIAlertAction actionWithTitle:@"Gallery"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * _Nonnull action) {
                                                              imagePickerController = [[UIImagePickerController alloc] init];
                                                              imagePickerController.delegate = self;
                                                              imagePickerController.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
                                                              [self presentViewController:imagePickerController animated:YES completion:nil];
                                                          }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleDestructive
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                             
                                                         }];
    [alertController addAction:cameraAction];
    [alertController addAction:pictureAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    [self.photoButton setImage:[info objectForKey:UIImagePickerControllerOriginalImage] forState:UIControlStateNormal];
    self.existsPhoto = YES;
    self.navigationItem.rightBarButtonItem.enabled = YES;
}

- (void)getHomeTown:(MKMapItem *)hometown {
    _hometown = hometown;
    self.hometownTextField.text = hometown.name;
    self.navigationItem.rightBarButtonItem.enabled = YES;
}

@end
